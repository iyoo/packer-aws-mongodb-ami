variable "mongodb_root_admin_password" {}
variable "mongodb_user_admin_password" {}
variable "mongodb_root_backup_password" {}

variable "mongodb_version" {
  default = "4.2"
}

variable "keep_ami_revisions" {
  default = "3"
}

variable "region" {
  default = "us-east-1"
}
