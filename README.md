# MongoDB AMI for AWS

Generate an AMI with MongoDB pre-installed for use on an AWS EC2 machine.

It is using the popular MongoDB [Ansible playbook](https://github.com/UnderGreen/ansible-role-mongodb) by UnderGreen and adds litterally no overhead.

This packer script will also keep only the latest 3 revisions of this AMI and removing older ones from your AWS account.

## Prerequisite

### Install

- [AWS CLI](https://aws.amazon.com/cli/)
- [Packer](https://packer.io/docs/install/index.html)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

> If you use homebrew on Mac OSX, you can just run `brew install awscli packer ansible`

### Configure

Configure AWS Cli with your credentials and default region

```sh
aws configure
```

## Usage

1. Set passwords for MongoDB admin users

   Either you set the following environment variables manually:

   - `PKR_VAR_mongodb_root_admin_password`
   - `PKR_VAR_mongodb_user_admin_password`
   - `PKR_VAR_mongodb_root_backup_password`

   Either you set random values automatically:

   ```sh
   chmod +x ./scripts/setenv.sh
   . ./scripts/setenv.sh
   ```

   > You can view the generated passwords with `chmod +x ./scripts/viewenv.sh && ./scripts/viewenv.sh`

2. Build the image

   ```sh
   packer build .
   ```

## Upgrading or Changing MongoDB options

[See here](https://github.com/UnderGreen/ansible-role-mongodb) for the list of available options for your MongoDB install

To tweak your installation:

- Either you modify the `playbook.yml` file directly
- Either you modify `mongodb.pkr.hcl` to pass variables from packer.

  > For example, look at how **mongodb_version** is passed

## Continuous Integration

This repo contains `.gitlab-ci.yml` to automatically deploy the AMI with GitLab CI.

Before you can use it, you'll need to set the following environment variables in the Gitlab UI or through the Gitlab API:

- AWS_DEFAULT_REGION
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

> In a CI environment, don't forget to also set the MongoDB password environment variables
