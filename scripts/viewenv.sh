# Set executable permissions:
# $ chmod +x ./viewenv.sh
#
# Run:
# $ ./viewenv.sh

echo PKR_VAR_mongodb_root_admin_password=$MONGODB_ROOT_ADMIN_PASSWORD
echo PKR_VAR_mongodb_user_admin_password=$MONGODB_USER_ADMIN_PASSWORD
echo PKR_VAR_mongodb_root_backup_password=$MONGODB_ROOT_BACKUP_PASSWORD
