# Set executable permissions:
# $ chmod +x ./setenv.sh
#
# Run with a . to source environment variables to the main shell:
# $ . ./setenv.sh

export PKR_VAR_mongodb_root_admin_password=$(openssl rand -base64 20)
export PKR_VAR_mongodb_user_admin_password=$(openssl rand -base64 20)
export PKR_VAR_mongodb_root_backup_password=$(openssl rand -base64 20)
