source "amazon-ebs" "mongodb" {
  ami_name      = "amzn2-ami-hvm-2.0-mongodb-${var.mongodb_version}.${formatdate("YYMMDDhhmmss", timestamp())}-x86_64-gp2"
  instance_type = "t2.micro"
  ssh_username  = "ec2-user"

  source_ami_filter {
    owners      = ["amazon"]
    most_recent = true

    filters {
      virtualization-type = "hvm"
      name                = "amzn2-ami-hvm-2.0.*-x86_64-gp2"
      root-device-type    = "ebs"
    }
  }

  tags {
    OS_Version = "Amazon Linux 2"
    // Build variables are currently not supported by Packer
    //Base_AMI_Name   = source.amazon-ebs.mongodb.ami_name
    Base_AMI_Name   = "amzn2-ami-hvm-2.0.*-x86_64-gp2"
    MongoDB_Version = var.mongodb_version
    Packer          = "true"
    Automation      = "true"
  }

  temporary_iam_instance_profile_policy_document {
    version = "2012-10-17"
    statement {
      effect   = "Allow"
      resource = "*"
      action = [
        "ec2:AttachVolume",
        "ec2:AuthorizeSecurityGroupIngress",
        "ec2:CopyImage",
        "ec2:CreateImage",
        "ec2:CreateKeypair",
        "ec2:CreateSecurityGroup",
        "ec2:CreateSnapshot",
        "ec2:CreateTags",
        "ec2:CreateVolume",
        "ec2:DeleteKeypair",
        "ec2:DeleteSecurityGroup",
        "ec2:DeleteSnapshot",
        "ec2:DeleteVolume",
        "ec2:DeregisterImage",
        "ec2:DescribeImageAttribute",
        "ec2:DescribeImages",
        "ec2:DescribeInstances",
        "ec2:DescribeRegions",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeSnapshots",
        "ec2:DescribeSubnets",
        "ec2:DescribeTags",
        "ec2:DescribeVolumes",
        "ec2:DetachVolume",
        "ec2:GetPasswordData",
        "ec2:ModifyImageAttribute",
        "ec2:ModifyInstanceAttribute",
        "ec2:ModifySnapshotAttribute",
        "ec2:RegisterImage",
        "ec2:RunInstances",
        "ec2:StopInstances",
        "ec2:TerminateInstances",
        "ec2:RequestSpotInstances",
        "ec2:CancelSpotInstanceRequests"
      ]
    }
    statement {
      effect   = "Allow"
      resource = "*"
      action = [
        "iam:PassRole",
        "iam:CreateInstanceProfile",
        "iam:DeleteInstanceProfile",
        "iam:GetRole",
        "iam:GetInstanceProfile",
        "iam:DeleteRolePolicy",
        "iam:RemoveRoleFromInstanceProfile",
        "iam:CreateRole",
        "iam:DeleteRole",
        "iam:PutRolePolicy",
        "iam:AddRoleToInstanceProfile"
      ]
    }
  }
}

build {
  sources = [
    "source.amazon-ebs.mongodb"
  ]

  provisioner "ansible" {
    playbook_file   = "./ansible/playbook.yml"
    galaxy_file     = "./ansible/requirements.yml"
    user            = "ec2-user"
    extra_arguments = ["-e", "mongodb_version = ${var.mongodb_version}"]
    ansible_env_vars = [
      "MONGODB_ROOT_ADMIN_PASSWORD=${var.mongodb_root_admin_password}",
      "MONGODB_USER_ADMIN_PASSWORD=${var.mongodb_user_admin_password}",
      "MONGODB_ROOT_BACKUP_PASSWORD=${var.mongodb_root_backup_password}"
    ]
  }

  post-processor "shell-local" {
    script = "./scripts/cleanup-ami-snapshots.sh"
    environment_vars = [
      "MONGODB_VERSION=${var.mongodb_version}",
      "KEEP_AMI_REVISIONS=${var.keep_ami_revisions}"
    ]
  }
}
